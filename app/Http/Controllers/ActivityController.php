<?php

namespace App\Http\Controllers;

use App\Models\AtusActivity;
use Illuminate\Http\Request;

class ActivityController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function random(){
        if (random_int(0, 1)){
            $flim = auth()->user()->getFlimByPreferences();
            $flim['type'] = 'flim';
            return $flim;
        }
        $activity = auth()->user()->getActivityByPreferences();
        $activity['type'] = 'activity';
        return $activity;
    }

    public function randomMainActivity(){
        $random = AtusActivity::take(600)->get()->shuffle()->take(30)
            ->filter(function($item){ return !is_null($item->code); })
            ->each(function($item){
                $item['type'] = 'activity';
            })->map(function($item){
            return ['type' => $item['type'], 'name' => $item->code->name];
        });

        $movies = collect();
        for ($i = 0; $i < 5; $i++){
            $movie = auth()->user()->getFlimByPreferences();
            $movie['type'] = 'flim';
            $movies->push($movie);
        }
        $activities = collect();
        for ($i = 0; $i < 5; $i++){
            $act = auth()->user()->getActivityByPreferences();
            $act['type'] = 'activity';
            $activities->push($act);
        }

        return $random->merge(collect([$movies, $activities])->flatten());
    }
}
