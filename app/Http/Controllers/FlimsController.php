<?php

namespace App\Http\Controllers;

use App\Models\Flims;
use Illuminate\Http\Request;

class FlimsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return auth()->user()->getActivityByPreferences();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Flims  $flims
     * @return \Illuminate\Http\Response
     */
    public function show(Flims $flims)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Flims  $flims
     * @return \Illuminate\Http\Response
     */
    public function edit(Flims $flims)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Flims  $flims
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flims $flims)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Flims  $flims
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flims $flims)
    {
        //
    }
}
