<?php

namespace App\Http\Controllers;

use App\Http\Requests\PreferenceRequest;
use App\Models\AtusActivity;
use App\Models\Flim;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class MeetAndGreetController extends Controller {
    public function __construct(){
        $this->middleware('auth');
    }


    public function index(){
        if (random_int(0, 1)){
            return Flim::limit(400)->get()->shuffle()->take(1)
                ->each(function($item){return $item['type'] = 'flim'; })
                ->each(function($item){
                    $temp = Http::get("https://www.omdbapi.com/?t=".
                        $item['title']."&apikey=e015378b")->json();

                    if ($temp['Response'] != 'False' and !is_null($temp['Poster'])){
                        $item['photo'] = $temp['Poster'];
                    }
                });
        }
        return auth()->user()->atus_activities()->take(1)
            ->each(function($item){return $item['type'] = 'activity'; });

    }

    public function store(PreferenceRequest $request){
        $affinity = ['affinity' => $request->grade];
        if ($request->type == "flim"){
            $request->user()->films()->attach($request->id, $affinity);
        } else if ($request->type == "activity"){
            $request->user()->activities()->attach($request->id, $affinity);
        }
    }
}
