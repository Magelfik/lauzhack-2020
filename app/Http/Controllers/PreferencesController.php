<?php

namespace App\Http\Controllers;

use App\Models\AtusActivity;
use App\Models\Flim;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PreferencesController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        $activities = auth()->user()->activities;
        $films = auth()->user()->films;

        return collect(['activities' => $activities, 'flims' => $films]);
    }

    public function getRandom(){
        $flims = Flim::limit(400)->get()->shuffle()->take(10);
        $activity = AtusActivity::limit(400)->get()->shuffle()->take(10);

        return collect([$flims, $activity])->flatten();
    }
}
