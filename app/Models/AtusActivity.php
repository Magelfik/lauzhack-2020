<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AtusActivity extends Model {
    use HasFactory;

    protected $table = "atus";
    protected $with = ['code'];

    public function code(){
        return $this->hasOne(AtusCode::class, 'id', 'activity');
    }

}
