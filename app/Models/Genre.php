<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Genre extends Model{
    use HasFactory;

    protected $primaryKey = "id";
    protected $keyType = "string";

    public function films(){
        return $this->belongsToMany("App\Models\Flim");
    }
}
