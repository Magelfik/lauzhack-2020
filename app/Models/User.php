<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'age'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Gets the movies liked by the user.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function films(){
        return $this->belongsToMany(Flim::class)->withPivot('affinity');
    }

    public function activities(){
        return $this->belongsToMany(AtusActivity::class, 'atus_user', 'user_id', 'atus_id')->withPivot('affinity');
    }

    /**
     * Returns, according to statistics, the potential activities interesting for this user.
     */
    public function atus_activities(){
        return AtusActivity::whereBetween('age', [$this->age - 3 , $this->age + 3])
            ->orderBy('duration', 'DESC')
            ->orderBy('age', 'DESC')
            ->limit(100)
            ->get()
            ->shuffle()
            ->unique('code.name')
            ->filter(function($activity){
                return !is_null($activity->code) && !is_null($activity->code->name);
            });
    }

    public function favouritesGenres() {
        $result = collect();
        $items = $this->films->map(function($item){
            return ['affinity' => $item->pivot->affinity, 'genre' => $item->genres->pluck('pivot.genre_id')];
        });

        foreach ($items as $item) {
            foreach($item['genre'] as $genre){
                $result->put($genre, is_null($result->get($genre)) ? 0 : $result->get($genre) + $item['affinity']);
            }
        }

        return $result->sort()->reverse();
    }

    public function favouritesActivities() {
        $items = $this->activities->map(function($item){
            return [$item->code->name => $item->pivot->affinity];
        });

        $result = collect();
        foreach ($items as $value) {
            foreach ($value as $key => $value) {
                $result[$key] = $value;
            }
        }

        return $result;
    }

    public function getFlimByPreferences() {
        $genre = $this->pc_rand_weighted($this->favouritesGenres());
        return Genre::findOrFail($genre)->films->shuffle()->first();
    }

    public function getActivityByPreferences() {
        $activity = $this->pc_rand_weighted($this->favouritesActivities());
        return AtusCode::whereName($activity)->firstOrFail();
    }

    private function pc_rand_weighted($numbers) {
        $total = 0;
        foreach ($numbers as $number => $weight) {
            $total += $weight;
            $distribution[$number] = $total;
        }
        $rand = mt_rand(0, $total - 1);
        foreach ($distribution as $number => $weights) {
            if ($rand < $weights) {
                return $number;
            }
        }
    }
}
