<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlimUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flim_user', function (Blueprint $table) {
            $table->foreignId('flim_id');
            $table->foreign('flim_id')->references('id')->on('flims');

            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('affinity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flim_user');
    }
}
