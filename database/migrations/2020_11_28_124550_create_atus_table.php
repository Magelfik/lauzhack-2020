<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atus', function (Blueprint $table) {
            $table->id();
            $table->integer('year');
            $table->integer('age');
            $table->integer('activity');
            $table->integer('duration');

            // ATTENTION. Désactivé car certaines activités n'ont pas de label (on les ignorera à la mano)
            // $table->foreign('activity')->references('id')->on('activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atus');
    }
}
