
CREATE DATABASE IF NOT EXISTS `employees` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `employees`;

CREATE TABLE IF NOT EXISTS employees (
	emp_no INT UNSIGNED NOT NULL,
    birth_date DATE NOT NULL,
    first_name varchar(50) NOT NULL,
    last_name varchar(60) NOT NULL,
    gender enum('F', 'M') NOT NULL,
    hire_date DATE NOT NULL,
    PRIMARY KEY (emp_no)
);

CREATE TABLE IF NOT EXISTS departments(
	dept_no varchar(4) NOT NULL,
    dept_name varchar(40) NOT NULL,
	PRIMARY KEY (dept_no)
);

CREATE TABLE IF NOT EXISTS dept_emp(
	emp_no INT UNSIGNED NOT NULL,
    dept_no varchar(4) NOT NULL,
    from_date date,
    to_date date,
    PRIMARY KEY (emp_no, dept_no),
    FOREIGN KEY (dept_no) REFERENCES departments(dept_no),
    FOREIGN KEY (emp_no) REFERENCES employees(emp_no)
);

CREATE TABLE IF NOT EXISTS dept_manager(
	dept_no varchar(4) NOT NULL,
	emp_no INT UNSIGNED NOT NULL,
    from_date date,
    to_date date,
    PRIMARY KEY (emp_no, dept_no),
    FOREIGN KEY (dept_no) REFERENCES departments(dept_no),
    FOREIGN KEY (emp_no) REFERENCES employees(emp_no)
);

CREATE TABLE IF NOT EXISTS salaries(
	emp_no INT UNSIGNED NOT NULL,
    salary INT UNSIGNED NOT NULL,
    from_date date NOT NULL,
    to_date date,
    PRIMARY KEY (emp_no, from_date),
    FOREIGN KEY (emp_no) REFERENCES employees(emp_no)
);

CREATE TABLE IF NOT EXISTS titles(
	emp_no INT UNSIGNED NOT NULL,
    title varchar(100) NOT NULL,
    from_date date,
    to_date date,
    PRIMARY KEY (title, from_date, emp_no),
    FOREIGN KEY (emp_no) REFERENCES employees(emp_no)
);