import datasets as ds

datas_raw = open(ds.NETFLIX_DATA_DIR, 'r')

flims = []
header = datas_raw.readline()

for c in datas_raw.readlines():
    flims.append(c)
datas_raw.close()

flims_end = []

for i in flims:
    flim = i.split(';')
    gs = flim[8].split(',')
    for i, w in enumerate(gs):
        if w[0] == ' ':
            gs[i] = w[1:]
    flims_end.append(
        {
            'title': flim[0],
            'release_date': int(flim[5]),
            'genres': gs
        }
    )

genres = []

for i in flims_end:
    for genre in i['genres']:
        if genre not in genres:
            if genre[0] == ' ':
                genre = genre[1:]
            genres.append(genre)

out = open('netflix.sql', 'w')

res = ['INSERT INTO `genres` (`id`) VALUES']
for i in genres:
    res.append('("{}"),'.format(i))
res[-1] = res[-1][:-1] + ';\n'

res.append('INSERT INTO `flims` (`title`, `release_date`) VALUES')
for i in flims_end:
    title = i['title']
    release_date = i['release_date']
    res.append('("{0}", {1}),'.format(title.replace('\"', ''), release_date))
res[-1] = res[-1][:-1] + ';\n'

res.append('INSERT INTO `flim_genre` (`flim_id`, `genre_id`) VALUES')
for i, el in enumerate(flims_end):
    for genre in el['genres']:
        res.append('({0}, "{1}"),'.format(i+1, genre))
res[-1] = res[-1][:-1] + ';'

out.writelines('\n'.join(res))
out.close()