genres = ["Children & Family Movies","Comedies","Stand-Up Comedy","Kids' TV","Crime TV Shows","International TV Shows","Spanish-Language TV Shows","International Movies","Sci-Fi & Fantasy","Thrillers","Docuseries","Science & Nature TV","Action & Adventure","Dramas","Cult Movies","Independent Movies","Romantic Movies","Documentaries","Horror Movies","Romantic TV Shows","TV Comedies","TV Dramas","TV Thrillers","TV Mysteries","British TV Shows","Music & Musicals","Reality TV","TV Action & Adventure","Anime Features","Teen TV Shows","Faith & Spirituality","Korean TV Shows","Anime Series","LGBTQ Movies","TV Horror","Movies","Stand-Up Comedy & Talk Shows","TV Sci-Fi & Fantasy","Classic Movies","Sports Movies","TV Shows","Classic & Cult TV"]

affinity = {
    "user1": {
        "Pulp Fiction": {
            "genres": ['Classic Movies', 'Cult Movies', 'Dramas'],
            "affinity": 2
        },
        "Breaking Bad": {
            "genres": ["Crime TV Shows", "TV Dramas", "TV Thrillers"],
            "affinity": 0
        },
        "El Camino: A Breaking Bad Movie": {
            "genres": ["Dramas", "Thrillers"],
            "affinity": 4
        }
    }
}

result = {}

for k in affinity['user1']:
    for genre in affinity['user1'][k]['genres']:
        if genre not in result:
            result[genre] = 0
        result[genre] += affinity['user1'][k]['affinity']

print(result)