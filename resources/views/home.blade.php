@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <main-menu :user="JSON.parse('{{ auth()->user() }}')"></main-menu>
            </div>
        </div>
    </div>
@endsection
