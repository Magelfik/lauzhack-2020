<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/meetandgreet', [\App\Http\Controllers\MeetAndGreetController::class, 'index']);
Route::post('/meetandgreet', [\App\Http\Controllers\MeetAndGreetController::class, 'store']);

Route::get("/preferences", [\App\Http\Controllers\PreferencesController::class, 'index']);
Route::get("/preferences/random", [\App\Http\Controllers\PreferencesController::class, 'getRandom']);

Route::get("/flimsuser", [\App\Http\Controllers\FlimsController::class, 'index']);

Route::get('/random', [\App\Http\Controllers\ActivityController::class, 'random']);
Route::get('/randomA', [\App\Http\Controllers\ActivityController::class, 'randomMainActivity']);
